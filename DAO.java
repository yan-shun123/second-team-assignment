package com.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DAO1 {


    public static final String url="jdbc:mysql://localhost:3306/Product?useUnicode=true&charaEncoding=utf8";
    public static final String user ="root";
    public static final String password="123456";
    //类第一次加载的时候执行一次
    static {

        try {

            //1.加载驱动
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }//有可能报错
    }


    //2.数据库链接
    public static Connection getConn() {


        Connection conn =null;

        try {

            conn= DriverManager.getConnection(url, user, password);

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }return conn;
    }

    public static void close(Connection conn,PreparedStatement pst) {
        try {
            //5.关闭数据库
            conn.close();
            pst.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

}


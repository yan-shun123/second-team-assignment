/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 80029
Source Host           : localhost:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 80029
File Encoding         : 65001

Date: 2022-05-03 23:01:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for consumer
-- ----------------------------
DROP TABLE IF EXISTS `consumer`;
CREATE TABLE `consumer` (
  `Uid` int NOT NULL,
  `Username` char(100) NOT NULL,
  `Coupon` int DEFAULT NULL,
  `Credit` int DEFAULT NULL,
  `Address` char(200) DEFAULT NULL,
  PRIMARY KEY (`Uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for orderform
-- ----------------------------
DROP TABLE IF EXISTS `orderform`;
CREATE TABLE `orderform` (
  `Oid` int NOT NULL,
  `Uid` int NOT NULL,
  `Wid` int NOT NULL,
  PRIMARY KEY (`Oid`),
  KEY `Uid` (`Uid`),
  KEY `Wid` (`Wid`),
  CONSTRAINT `orderform_ibfk_1` FOREIGN KEY (`Uid`) REFERENCES `consumer` (`Uid`),
  CONSTRAINT `orderform_ibfk_2` FOREIGN KEY (`Wid`) REFERENCES `wares` (`Wid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for rider
-- ----------------------------
DROP TABLE IF EXISTS `rider`;
CREATE TABLE `rider` (
  `Rid` int NOT NULL,
  `Rname` char(50) NOT NULL,
  `Oid` int DEFAULT NULL,
  PRIMARY KEY (`Rid`),
  KEY `Oid` (`Oid`),
  CONSTRAINT `rider_ibfk_1` FOREIGN KEY (`Oid`) REFERENCES `orderform` (`Oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `ShopId` int NOT NULL,
  `SopName` char(200) NOT NULL,
  `ShopKeeper` char(50) NOT NULL,
  `ShopTel` int NOT NULL,
  PRIMARY KEY (`ShopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for wares
-- ----------------------------
DROP TABLE IF EXISTS `wares`;
CREATE TABLE `wares` (
  `Wid` int NOT NULL,
  `Wname` char(200) NOT NULL,
  `IMG` longblob,
  `Price` double NOT NULL,
  `Market_price` double DEFAULT NULL,
  `Content` char(200) DEFAULT NULL,
  `ShopId` int NOT NULL,
  PRIMARY KEY (`Wid`),
  KEY `ShopId` (`ShopId`),
  CONSTRAINT `wares_ibfk_1` FOREIGN KEY (`ShopId`) REFERENCES `store` (`ShopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

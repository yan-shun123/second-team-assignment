package javashop.shop;

public class Wares {

    private String name;
    private String spec;
    private double price;
    private double market_price;
    private String content;

    public Wares() {
    }

    public Wares(String name, String spec, double price, double market_price, String content) {
        this.name = name;
        this.spec = spec;
        this.price = price;
        this.market_price = market_price;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMarket_price() {
        return market_price;
    }

    public void setMarket_price(double market_price) {
        this.market_price = market_price;
    }

    public String getContent() {
        return content;
    }

   

    @Override
    public String toString() {
        return name;
    }
}

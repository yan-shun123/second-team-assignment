package test;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class select {
    void readFile(ActionEvent event) throws SQLException {
        String str = tfName.getText().trim();
        if (!str.isEmpty()) {
            Statement statement = conn.createStatement();

            //创建mysql执行语句
            String sql = "SELECT * FROM Wares WHERE name like ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,"%" + str + "%");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                if (!rs.getString("name").equals("")){
                    for (int i = 0;i<list.size();i++){
                        if (rs.getString("name").equals(list.get(i).getName())) {
                            lvAll.getSelectionModel().select(i);
                            break;
                        }
                    }
                }else {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Git实战");
                    alert.setHeaderText("请注意！");
                    alert.setContentText("找不到商品！");
                    alert.show();
                }

            }
        }

    }
}

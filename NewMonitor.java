package javashop.shop;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: Monitor
 * @Description: 待写
 * @Author 杯酒
 * @Date 2022/3/18
 * @Version 1.0
 */
public class NewMonitor {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public NewMonitor() {
    }

    public NewMonitor(String url) {
        this.url = url;
    }

    List<Wares> list = new ArrayList<>();

    Connection conn = null;
    PreparedStatement ps = null;

    /**
     * 通过正则表达式去除引号
     *
     * @param data
     * @param name
     * @return
     */
    private String regex(JsonObject data, String name) {
        String regex = "\"+";
        String result = data.get(name).toString().replaceAll(regex, "");
        return result;
    }

    /**
     * 显示具体的内容
     */
    public Wares context() {
        JsonObject data = createJsonObject();
        String spec = regex(data, "spec");
        Double price = Double.valueOf(regex(data, "price")) / 100;
        Double market_price = Double.valueOf(regex(data, "market_price")) / 100;
        String title = regex(data, "share_title");
        String content = regex(data, "share_content");

        return new Wares(title,spec,price,market_price,content);
    }

    /**
     * 从json字符串中取出JsonObject类型的"data"
     *
     * @return
     */
    private JsonObject createJsonObject() {
        Object obj = loadJson();
        JsonObject returnData = new JsonParser().parse((String) obj).getAsJsonObject().getAsJsonObject("data");
        return returnData;
    }

    /**
     * 读取url的json字符串内容
     *
     * @return
     */
    private String loadJson() {
        String url = getUrl();
        if (!url.trim().isEmpty()) {
            StringBuilder json = new StringBuilder();
            try {
                URL urlObject = new URL(url);
                URLConnection uc = urlObject.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                String inputLine = null;
                while ((inputLine = in.readLine()) != null) {
                    json.append(inputLine);
                }
                in.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return json.toString();
        } else {
            return null;
        }
    }

    public void add(Wares wares){
        try {
            conn = DBConnection.getConn();
            String sql = "Insert into Wares (name,spec,price,market_price,content) values(?,?,?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1,wares.getName());
            ps.setString(2,wares.getSpec());
            ps.setDouble(3,wares.getPrice());
            ps.setDouble(4,wares.getMarket_price());
            ps.setString(5,wares.getContent());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBConnection.close(conn,ps);
        }
    }

}

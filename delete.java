public class delete {
    public void deleteClick(ActionEvent actionEvent) {
    int num = lvAll.getSelectionModel().getSelectedIndex();
    delete(num);
    taMessage.clear();
}

    public void delete(int num){
        try {
            //调用写好的数据库驱动类
            conn=DBConnection.getConn();
            //3.数据库操纵对象
            String sql="delete from Wares where name=?";
            ps= conn.prepareStatement(sql);
            //给参数赋值
            ps.setString(1,list.get(num).getName());//parameterIndex 代表第几个
            //4.执行SQL
            ps.executeUpdate();
            list.remove(num);



        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            //5.关闭数据库


            DBConnection.close(conn, ps);
        }


    }

}
